<?php
require_once 'WaveFile.php';
session_start();
if (isset($_GET['file']) && $_GET['file']) {
	header('Content-type: audio/x-wav');
	$basedir = session_id();
	readfile($basedir.'/'.$_GET['file'].'.wav');
	die;
} 
if (isset($_GET['pic']) && $_GET['pic']) {
	header('Content-type: image/png');
	if ($_GET['width']) {
		$width = (int)$_GET['width'];
	} else {
		$width = 1000;
	}
	$pic = imagecreatetruecolor($width, 200);
	$background =  imagecolorallocate($pic, 255,255,255);
	$line = imagecolorallocate($pic, 0,0,255);
	$basedir = session_id();
	$wave = new WaveFile($basedir.'/wave.wav');
	$wave -> parseHeaders();
	$lineStart = 100;
	if ($_GET['method']) {
		$method = $_GET['method'];
	} else {
		$method = "avg";
	}
	foreach ($wave -> resample($width, $method) as $index => $sample) {
		imageline($pic, $index, $lineStart, $index, $lineStart+($sample['positive']/350), $line); 
		imageline($pic, $index, $lineStart, $index, $lineStart+($sample['negative']/350), $line); 
	}	
	imagepng($pic);
	die;
}
form();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	handlePostRequest();
	drawTest();
}
function form() {
	?>
		<form enctype="multipart/form-data" method="POST">
			FLAC file: <input type="file" name="inputFile" />
			<button type="submit">OGNIA</button>
		</form>
<?php
#	$basedir = session_id();
#	try {
#	$wave = new WaveFile($basedir.'/wave.wav');
#	$wave -> parseHeaders();
##	var_dump($wave -> getFormatInfo());
#	#	var_dump(bin2hex($wave -> readBlock()));
#	#
#	echo '<pre>';
#	var_dump($wave);// -> nextBlock();
#	var_dump($wave->estimateLength());
#	var_dump($wave -> resample(10000));
#	} catch (Exception $e) {
#		var_dump($e);
#	}
}


function handlePostRequest() {
	$basedir = session_id();
	mkdir($basedir);
saveAsWave($_FILES['inputFile']['tmp_name'], $basedir.'/wave.wav');
saveThroughMP3VBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp30.wav', 0, 160);
saveThroughMP3VBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp34.wav', 4, 160);
saveThroughMP3VBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp39.wav', 9, 160 );
saveThroughMP3CBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp3320.wav', 320);
saveThroughMP3CBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp3256.wav', 256);
saveThroughMP3CBR($_FILES['inputFile']['tmp_name'], $basedir.'/mp364.wav', 64);
}

function saveAsWave($path, $destPath) {
	exec('cat "'.$path.'" | ffmpeg -loglevel panic -i /dev/stdin -f wav "'.$destPath.'"');
}
function saveThroughMP3VBR($path, $destPath, $quality, $bitrate) {
	exec($cmd = 'cat "'.$path.'" | ffmpeg -loglevel panic -i /dev/stdin -f wav - | lame -quiet -m s -V'.$quality.' -h -b '.$bitrate.' --vbr-new - - | ffmpeg  -loglevel panic -i /dev/stdin -f wav - > '.str_replace(' ', '\\ ', $destPath).'');
	echo $cmd, '<br />';
}
function saveThroughMP3CBR($path, $destPath, $bitrate) {
	exec($cmd = 'cat "'.$path.'" | ffmpeg -loglevel panic -i /dev/stdin -f wav - | lame -quiet -m s -cbr -h -b '.$bitrate.' - - | ffmpeg  -loglevel panic -i /dev/stdin -f wav - > '.str_replace(' ', '\\ ', $destPath).'');
	echo $cmd, '<br />';
}
function drawTest() {
	$types = getNames();
?>
	<img src="/index.php?pic=1" alt="pic" /> <br />
<?php
	shuffle($types);
	foreach($types as $type) {
		?>
			<audio src="/index.php?file=<?=$type?>" type="audio/wav" controls="controls"></audio><br />
		<?php
	}
}
function getNames() {
	return [
		'wave',
		'mp30',
		'mp34',
		'mp39',
		'mp3320',
		'mp3256',
		'mp364'
	];
}
